package com.itfitness.optfragment;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import java.util.ArrayList;

/**
 * @ProjectName: HeaderLayout
 * @Package: com.itfitness.headerlayout.fragments
 * @ClassName: TestFragment
 * @Description: java类作用描述
 * @Author: 作者名
 * @CreateDate: 2020/11/10 22:35
 * @UpdateUser: 更新者：itfitness
 * @UpdateDate: 2020/11/10 22:35
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */
public class TestFragment extends LazyFragment {
    private RecyclerView rv;
    private String TAG = "";
    public TestFragment(String tag) {
        this.TAG = tag;
    }

    @Override
    void initView(View view) {
        rv = view.findViewById(R.id.rv);
    }

    @Override
    int getLayoutRes() {
        return R.layout.fragment_test;
    }

    @Override
    void onLoadData() {
        Log.e(TAG,"数据加载");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initDatas();
            }
        },3000);
    }

    @Override
    void onStopLoadData() {
        Log.e(TAG,"数据停止加载");
    }

    private void initDatas() {
        ArrayList<String> datas = new ArrayList<>();
        for(int i = 0 ; i < 50 ; i ++){
            datas.add(TAG + "条目" + i);
        }
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setAdapter(new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_test,datas) {
            @Override
            protected void convert(BaseViewHolder helper, String item) {
                helper.setText(R.id.tv_name,item);
            }
        });
    }
}
