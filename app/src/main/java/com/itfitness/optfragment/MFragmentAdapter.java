package com.itfitness.optfragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * @ProjectName: OptFragment
 * @Package: com.itfitness.optfragment
 * @ClassName: MFragmentAdapter
 * @Description: java类作用描述
 * @Author: 作者名
 * @CreateDate: 2020/11/14 21:37
 * @UpdateUser: 更新者：itfitness
 * @UpdateDate: 2020/11/14 21:37
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */
public class MFragmentAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragments;
    private ArrayList<String> titles;

    public MFragmentAdapter(@NonNull FragmentManager fm, ArrayList<Fragment> fragments, ArrayList<String> titles) {
        super(fm);
        this.fragments = fragments;
        this.titles = titles;
    }
    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }
}
